import argparse
import tdclient
import os
import csv
import datetime, time



def valid_date(s):
    try:
        datetime.datetime.strptime(s, "%Y-%m-%d")
        return s
    except ValueError:
        msg = "Not a valid date: '{0}'.".format(s)
        raise argparse.ArgumentTypeError(msg)


def get_args():
    parser = argparse.ArgumentParser(description='TD python client table query tool')
    parser.add_argument(
        '-db', '--db_name', type=str, help='db name', required=True)
    parser.add_argument(
        '-table', '--table_name', type=str, help='table name', required=True)
    parser.add_argument(
        '-col', '--col_list', type=str, help='column list', required=False, default=None)
    parser.add_argument(
        '-m', '--min_time', type=valid_date, help='min time', required=False, default=None)
    parser.add_argument(
        '-M', '--max_time', type=valid_date, help='max time', required=False, default=None)
    parser.add_argument(
        '-e', '--engine', type=str, help='engine', required=False, choices=['hive','presto'], default='presto')
    parser.add_argument(
        '-f', '--format', type=str, help='output format', required=False, choices=['csv','tabular'],  default='tabular')
    parser.add_argument(
        '-l', '--limit', type=int, help='query limit', required=False, default=None)

    args = parser.parse_args()
    db = args.db_name
    table = args.table_name
    col_list= args.col_list if args.col_list else '*'
    min_ts = int(time.mktime(datetime.datetime.strptime(args.min_time, "%Y-%m-%d").timetuple())) if args.min_time else 'NULL'
    max_ts = int(time.mktime(datetime.datetime.strptime(args.max_time, "%Y-%m-%d").timetuple())) if args.max_time else 'NULL'
    engine = args.engine
    format = args.format
    limit = args.limit

    return db, table, col_list, min_ts, max_ts, engine, format, limit




def run():
    db, table, col_list, min_ts, max_ts, engine, format, limit = get_args()
    apikey = os.getenv("TD_API_KEY")

    if format and format == 'csv':
        delimiter = ','   #csv
    else:
        delimiter = '|'   #tabular

    with tdclient.Client(apikey) as td:
        query_des_table = 'describe contentwise.vod_stream;'
        job = td.query(db, query_des_table, type=engine)
        job.wait()
        header = []

        if col_list != '*':          # header with all columns
            cols = col_list.split(',')
            for c in cols:
                header.append(c)
        else:                        # header with selected columns
            for row in job.result():
                col = row[0].encode('UTF8')
                header.append(col)


        query_sel_table = "SELECT {col_list} FROM {table_name} WHERE TD_TIME_RANGE(licensewindow, {min}, {max})"\
                .format(col_list=col_list, table_name=table, min=min_ts, max=max_ts)

        if limit:
            query_sel_table += ' LIMIT %s' %limit


        print ':> %s' %query_sel_table
        job = td.query(db, query_sel_table, type=engine)
        job.wait()

        with open('output', "wb") as file:
            writer = csv.writer(file,delimiter=delimiter)
            writer.writerow(header)
            for row in job.result():
                content = []
                for x in row:
                    if type(x) is unicode:
                        item = x.encode('UTF8')
                    elif type(x) is int:
                        item = str(x)
                    content.append(item)
                writer.writerow(content)
    return 0




if __name__ == '__main__':
    run()