It requires python 2.7 version to run the application.

- Install required packages. You can find requirements.txt in the td-utility repository

pip install -r requirements.txt


- Export environment variable for api key. The API key can be found in the account page

export TD_API_KEY=<API_KEY>

The API_KEY to use for this demo is "9521/fe5ba50fab50b5eaf85df82e429c2330570cdac7"

- Run the App.

python query.py -db <db_name> -table <table_name> -c <column_list> -m <min_ts> -M <max_ts> -e <engine> -f <format> -l <limit>

Please refer to the file "testcase" for all of the possible use cases.
The query output can be found in td-utility/output